const { Kafka } = require("kafkajs");
const fs = require("fs");
const readline = require("readline");
const { EventEmitter } = require("events");

const newTweetEmitter = new EventEmitter();

const kafka = new Kafka({
  clientId: "app",
  brokers: ["0.0.0.0:29092"],
  logLevel: 1,
});

const timeout = (timout) =>
  new Promise((res) => {
    setTimeout(() => res(null), timout);
  });

const producer = kafka.producer();

async function readTweetsFromFile() {
  try {
    const fileStream = fs.createReadStream("twcs.csv");

    const readlineInterface = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity,
    });

    let isFirstLine = true;

    for await (const line of readlineInterface) {
      // skip first line of csv-file with metadata
      if (isFirstLine) {
        isFirstLine = !isFirstLine;
        continue;
      }

      const chunks = line.split(",");
      chunks[3] = new Date().toString();
      console.log(chunks[0])
      newTweetEmitter.emit("new-tweet", chunks.join(","));

      await timeout(100);
    }
  } catch (e) {
    console.error(e);
  }
}

async function initNewTwitsListener() {
  newTweetEmitter.on("new-tweet", async (tweet) => {
    console.log(tweet.split(',')[0]);
    try {
      await producer.connect();
      await producer.send({
        topic: "test-topic",
        messages: [{ key: "tweet", value: tweet }],
      });
    } catch (e) {
      console.error(e);
    }
  });
}

const run = async () => {
  await initNewTwitsListener();
  await readTweetsFromFile();
};

run().catch(console.error);
